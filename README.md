[![pipeline status](https://gitlab.com/murtis/flight_review/badges/master/pipeline.svg)](https://gitlab.com/murtis/flight_review/commits/master)

run as:

```
docker run --rm -ti \
  --name flight_review \
  --network=host \
  murtis/flight_review
```

The dockerfile will inform you what IP to connect to.
